package controllers

import play.api.mvc._
import play.api.test._
import play.api.test.Helpers._
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.Application
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import org.scalatest._
import scala.concurrent.Future

class HomeControllerSpec extends PlaySpec
  with GuiceOneAppPerSuite
  with Results {

  //why it does not work...
  implicit override lazy val app = new GuiceApplicationBuilder()
    .configure(Map("setting.socket.ip" -> "168.95.0.1")).build

  def controller(implicit app: Application): HomeController = 
    Application.instanceCache[HomeController].apply(app)

  
  "HomeController" should {
    "render the index page" in {
      val result: Future[Result] = controller.index().apply(FakeRequest())
      
      status(result) mustBe OK
      contentType(result) mustBe Some("text/plain")
      contentAsString(result) must include ("168.95.0.1")
    }
  }
}