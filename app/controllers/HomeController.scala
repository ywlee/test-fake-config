package controllers

import play.api._
import play.api.mvc._
import com.typesafe.config.ConfigFactory

class HomeController extends Controller {
  def index = Action {
  	val ip = ConfigFactory.load.getString("setting.socket.ip") 
    Ok(ip)
  }
}
